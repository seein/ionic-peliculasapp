import { Component } from '@angular/core';
import { MoviesService } from '../services/movies.service';
import { Pelicula } from '../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { DetalleComponent } from '../components/detalle/detalle.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

    textoBuscar = '';
    buscando = false;
    peliculas: Pelicula[] = [];
    ideas = ['Spiderman', 'Avengers', 'Superman', 'La vida es bella', 'Car'];

    constructor(private _moviesServices: MoviesService,
                private modalController: ModalController){}

    buscar(event){
        const valor: string = event.detail.value;
        this.buscando = true;
        
        if(valor.length > 0){
            this._moviesServices.buscarPelicula(valor).subscribe( resp => {
                console.log(resp);
                this.peliculas = resp['results'];
                this.buscando = false;
            });
        } else {
            this.peliculas = [];
            this.buscando = false;
        }
    }

    async verDetalle(id: string){
        const modal = await this.modalController.create({
            component: DetalleComponent,
            componentProps: {
                id
            }
        });

        modal.present();
    }

}
