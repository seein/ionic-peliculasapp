# PelisApp - Ionic

_Aplicación que muestra las películas que se encuentran en estreno. Fue desarrollado siguiendo un curso de Ionic en una plataforma educativa_

### Pre-requisitos 📋

* Angular 8
* Node.js - npm

### Instalación 🔧

_En el directorio del proyecto ingresar el siguiente comando a través de una terminal_

* npm install

## Despliegue 📦

_Ejecutar el siguiente comando para correr el proyecto_

* ionic serve